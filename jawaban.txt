1. Membuat database

create database myshop;

2. Membuat table di dalam database

create table users(id int NOT NULL auto_increment, 
    -> name varchar(255), 
    -> email varchar(255), 
    -> password varchar(255),
    -> primary key(id));
create table categories(id int not null auto_increment, 
    -> name varchar(255), 
    -> primary key(id));
create table items(id int not null auto_increment, 
    -> name varchar(255), 
    -> description varchar(255), 
    -> price int, 
    -> stock int, 
    -> category_id int, 
    -> primary key(id), 
    -> foreign key(category_id) references categories(id));

3. Memasukkan data pada tabel

insert into users(name, email, password) values 
    -> ("John Doe", "john@doe.com", "john123"), 
    -> ("Jane Doe", "jane@doe.com", 'jenita123');
insert into categories (name) values ("gadget"),
    -> ("cloth"),
    -> ("men"),
    -> ("women"),
    -> ("branded");
insert into items(name, description, price, stock, category_id) values
    -> ("Sumsang B50", "hape keren dari merek sumsang", 4000000, 100, 1),
    -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Mengambil data dari database

a. Query tabel users
select id, name, email from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+

b. Query mengambil data items
- select * from items where price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang B50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+

-select * from items where name like '%sang%';
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | price   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang B50 | hape keren dari merek sumsang | 4000000 |   100 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+


c. Query menampilkan data items join dengan kategori
select items.name, description, price, stock, category_id, categories.name as kategori 
    -> from items inner join categories on items.category_id = categories.id order by stock desc;
+-------------+-----------------------------------+---------+-------+-------------+----------+
| name        | description                       | price   | stock | category_id | kategori |
+-------------+-----------------------------------+---------+-------+-------------+----------+
| Sumsang B50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget   |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth    |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget   |
+-------------+-----------------------------------+---------+-------+-------------+----------+

5. Mengubah data dari database

update items set price = 2500000 where id = 1;
